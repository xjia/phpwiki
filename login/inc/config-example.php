<?php
define('DB_NAME', 'users');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_HOST', 'localhost');

define('HOST_URL', 'http://localhost');
define('SITE_BASE', '/wiki');
define('SITE_TITLE', 'phpwiki');
define('LOGIN_BASE', '/wiki/login');
